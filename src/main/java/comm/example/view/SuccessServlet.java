package comm.example.view;

import comm.example.model.League;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class SuccessServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }
    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out=resp.getWriter();
        League league=(League) req.getAttribute("LEAGUE");
        out.println("success page");
        out.println("League ID: "+league.getLeagueId()+"</br>");
        out.println("League Title: "+league.getTitle()+"</br>");
        out.println("League Year: "+league.getYear()+"</br>");
        out.println("League Season: "+league.getSeason()+"</br>");
        out.println("<a href='index.jsp'>Go To Home Page</a>");
    }
}
