package comm.example.view;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

public class ErrorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }
    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out=resp.getWriter();
        out.println("error page");
        List<String> list=(List<String>)req.getAttribute("ERROR");
        Iterator<String> iterator=list.iterator();
        out.println("<br/><u>please correct the bellow errors</u><br/>");
        while (iterator.hasNext())
        {
            out.println("<font color=red>"+iterator.next().toString()+"</br>");
        }
        out.println("</font>");
        out.println("<br/><a href='index.jsp'>Go To Home Page</a>");
    }
}

